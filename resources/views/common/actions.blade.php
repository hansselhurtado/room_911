<ul class="table-controls ">
    <li>
        <button type="button" title="update" class="btn btn-dark"  wire:click="edit({{$elemen->id}}, 1)" data-toggle="modal" data-target="#modalEmployee">
            <i class="la la-edit la-lg"></i>
        </button>
    </li>

    @if (Auth::user()->role->name == "Admin ROOM_911")
        <li>
            <button type="button" title="Enable/Disabled Acccess" class="btn btn-dark" onclick="handleState({{$elemen->id}}, '{{$elemen->status}}', 1)">
                <i class="la la-exchange la-lg"></i>
            </button>
        </li>
        <li>
            <button type="button" title="delete" class="btn btn-dark" onclick="deleteEmployee({{$elemen->id}}, 1)">
                <i class="la la-trash la-lg"></i>
            </button>
        </li>
    @endif
    @if ($url == "home")
        <li>
            <button type="button" title="history" class="btn btn-dark" wire:click="edit({{$elemen->id}}, 2)">
                <i class="la la-history la-lg"></i>
            </button>
        </li>
    @endif
    @if ($url == "users")
        <li>
            <button type="button" title="changed passwor" class="btn btn-dark" wire:click="edit({{$elemen->id}}, 1)" data-toggle="modal" data-target="#modalChangedPassword">
                <i class="la la-history la-lg"></i>
            </button>
        </li>
    @endif
</ul>
