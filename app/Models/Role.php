<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'roles';

    const ACTIVE = 'ACTIVO';


    protected $fillable = [
        'name',
        'status'
    ];

    const NAME = [
        'Admin ROOM_911',
        'Assistant ROOM_911',
    ];

    public function users() {
        return $this->hasMany(User::class);
    }

    // SCOPE

    public function scopeActive($query)
    {
        return $query->where('status', Role::ACTIVE);
    }
}
