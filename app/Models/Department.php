<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'departments';

    const ACTIVE = 'ACTIVO';


    protected $fillable = [
        'name',
        'status'
    ];

    const NAME = [
        'production department A',
        'production department B',
        'production department C',
    ];

    public function employees() {
        return $this->hasMany(Employee::class);
    }

    // SCOPE

    public function scopeActive($query)
    {
        return $query->where('status', Department::ACTIVE);
    }

    //  ACCESSORS
    public function getNameAttribute($value) {
        return ucfirst($value);
    }
}
