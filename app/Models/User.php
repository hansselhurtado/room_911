<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'user_name',
        'role_id',
        'password',
        'last_access',
        'status',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role() {
        return $this->belongsTo(Role::class,'role_id');
    }

    // scope
    public function scopeHandleAll($query)
    {
        return $query->where('id', '>', 1);
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('name','like', '%'.$search.'%')
                        ->orWhere('id','like', '%'.$search.'%')
                        ->orWhere('email','like', '%'.$search.'%')
                        ->orWhere('user_name','like', '%'.$search.'%')
                        ->orWhere('last_access','like', '%'.$search.'%')
                        ->orWhere('status','like', '%'.$search.'%')
                        ->OrwhereHas('role', function($q) use ($search){
                            $q->where('name','like', '%'.$search.'%');
                        });
    }

    public function scopeAuthUser($query, $id)
    {
        return $query->where('id', $id);
    }
}
