<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccessAction extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'access_actions';

    const ACTIVE = 'ACTIVO';


    protected $fillable = [
        'name',
        'employee_id',
        'document_number',
        'success'
    ];

    public function employee() {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    // scope
    public function scopeDateAccess($query, $dateFrom, $dateTo){
        return $query->whereDate('created_at', '>=', $dateFrom)
                    ->whereDate('created_at', '<=', $dateTo);
    }

    public function scopeUniqueDateAccess($query, $date){
        return $query->whereDate('created_at', $date);
    }

    public function scopeGetAccesEmployye($query, $id){
        return $query->where('employee_id', $id);
    }
    //  ACCESSORS
    public function getFullCreatedAtAttribute() {
        return Carbon::parse($this->created_at)->toFormattedDateString().' - '.Carbon::parse($this->created_at)->format('h:i:s A');
    }


}
