<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'employees';

    const ACTIVE = 'ACTIVO';


    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'document_number',
        'department_id',
        'status'
    ];

    public function department() {
        return $this->belongsTo(Department::class,'department_id');
    }

    public function accessActions() {
        return $this->hasMany(AccessAction::class);
    }

    // scope
    public function scopeHandleAll($query)
    {
        return $query->where('id', '>', 0);
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('first_name','like', '%'.$search.'%')
                        ->orWhere('id','like', '%'.$search.'%')
                        ->orWhere('middle_name','like', '%'.$search.'%')
                        ->orWhere('last_name','like', '%'.$search.'%')
                        ->orWhere('document_number','like', '%'.$search.'%')
                        ->orWhere('last_name','like', '%'.$search.'%')
                        ->orWhere('status','like', '%'.$search.'%');
    }

    public function scopeDocumenNumber($query, $document){
        return $query->where('document_number', $document);
    }

    public function scopeDepartment($query, $id){
        return $query->where('department_id', $id);
    }

    public function scopeDateAccess($query, $dateFrom, $dateTo){
        return $query->whereHas('accessActions', function($q) use ($dateFrom, $dateTo){
            $q->whereDate('created_at', '>=', $dateFrom);
            $q->whereDate('created_at', '<=', $dateTo);
            $q->where('success', 'YES');
        });
    }

    public function scopeUniqueDateAccess($query, $date){
        return $query->whereHas('accessActions', function($q) use ($date){
            $q->whereDate('created_at', $date);
            $q->where('success', 'YES');
        });
    }

    //  ACCESSORS
    public function getFirstNameAttribute($value) {
        return ucfirst($value);
    }

    public function getLastNameAttribute($value) {
        return ucfirst($value);
    }

    public function getMiddleNameAttribute($value) {
        return ucfirst($value);
    }

    public function getFullNameAttribute() {
        return $this->first_name.' '.$this->middle_name.' '.$this->last_name;
    }

    public function getLastAccessAttribute() {
        $access = AccessAction::where('employee_id', $this->id)->where('success', 'YES')->get()->last();
        if ($access) {
            return Carbon::parse($access->created_at)->toFormattedDateString().' - '.Carbon::parse($access->created_at)->format('h:i:s A');
        }
        return 'not record';
    }

    public function getLastAccessTotalAttribute() {
        return AccessAction::where('employee_id', $this->id)->where('success', 'YES')->count();
    }

    public function getFullCreatedAtAttribute() {
        return Carbon::parse($this->created_at)->toFormattedDateString().' - '.Carbon::parse($this->created_at)->format('h:i:s A');
    }

    public function getFullUpdatedAtAttribute() {
        return Carbon::parse($this->updated_at)->toFormattedDateString().' - '.Carbon::parse($this->updated_at)->format('h:i:s A');
    }

}
