<?php

namespace App\Imports;

use App\Models\Employee;

use Maatwebsite\Excel\Concerns\{Importable, ToModel, WithHeadingRow};

class EmployeeImport implements ToModel, WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Employee([
            'first_name'        => $row['first_name'],
            'middle_name'       => $row['middle_name'],
            'last_name'         => $row['last_name'],
            'document_number'   => $row['document_number'],
            'department_id'     => $row['department_id'],
        ]);
    }

    public function rules()
    {
        return [
            'first_name'                => 'bail|required|min:3|max:20|string',
            'middle_name'               => 'bail|nullable|string|min:3|max:50',
            'last_name'                 => 'bail|required|string|min:3|max:50',
            'document_number'           => 'bail|required|numeric|digits_between:7,12|unique',
            'department'                => 'bail|required|not_in:Elegir',
        ];
    }
}
