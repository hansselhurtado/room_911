<?php

namespace App\Http\Livewire;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;

class UserController extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';//poder utilizar la paginación en laravel 8


    public $roles, $selected_id, $user, $name, $email, $user_name, $password, $role = 'Elegir', $status ='ACTIVE', $search, $action = 1, $edit = 1;
    public $pagination = 5;
    public $newPassword, $repeatNewPassword;

    public function mount(){
        $this->roles    = Role::active()->get();
        $this->url      = Route::current()->getName();

    }

    public function render()
    {
        $users = User::handleAll();
        if (Auth::user()->role->name != "Admin ROOM_911") {
            $users = $users->authUser(Auth::user()->id);
        }
        if ($this->search) {
            $users->Search($this->search);
        }
        return view('livewire.users.component',[
            'info'  => $users->orderBy('name')->paginate($this->pagination),
        ]);
    }

    public function handleAction($action)
    {
        $this->handleReset($action);
    }

    public function StoreOrUpdate($action)
    {
        // dd($this->status);
        $this->validate([
                'name'                => 'bail|required|min:3|max:20|string',
                'email'               => 'bail|required|email|min:3|max:100',
                'password'            => 'bail|required|string',
                'user_name'           => 'bail|required|string|min:3|max:50|unique:users,user_name,'.$this->selected_id,
                'role'                => 'bail|required|not_in:Elegir',
                'status'              => 'bail|required|not_in:Elegir',
            ]
        );

        $find = [
            'id'  => $this->selected_id,
        ];
        $data = [
            'name'            => $this->name,
            'email'           => $this->email,
            'user_name'       => $this->user_name,
            'role_id'         => $this->role,
            'status'          => $this->status,
        ];


        if ($this->selected_id) {
            $updateOrCreate = 'update';
        }else{
            $updateOrCreate     = 'created';
            $data['password']   = Hash::make($this->password);
        }

        $user = User::updateOrCreate($find, $data);

        $this->handleReset($action);
        $this->emit('modalsClosed');//emit to js closed modals
        $this->emit('msgok','User '.$user->name.', to '.$updateOrCreate);

    }

    public function handlePassword(User $user, $action){
        if ($this->newPassword) {
            if ($this->newPassword == $this->repeatNewPassword) {
                $user->update([
                    'password' => Hash::make($this->newPassword)
                ]);
                $this->emit('modalsClosed');
                $this->emit('msgok','User '.$user->name.', changed password');
                $this->handleReset($action);
            }else{
                $this->emit('msg-error','Passwords do not match');
            }
        }else{
            $this->emit('msg-error','Incorrect value, please enter a password');
        }
    }

    public function clearPasswords(){
        $this->newPassword          = null;
        $this->repeatNewPassword    = null;
    }

    // listen for events and execute requested action
    protected $listeners = ['handleState','deleteUser'];

    public function handleState(User $user, $status, $action ){
        $user->status = $status;
        $user->save();
        $this->handleReset($action);
    }

    public function deleteUser(User $user, $action ){
        $user->delete();
        $this->handleReset($action);
    }

    public function edit(User $user, $action)
    {
    	$this->user                 = $user;
    	$this->selected_id          = $user->id;
    	$this->name                 = $user->name;
    	$this->email                = $user->email;
    	$this->password             = $user->password;
    	$this->role                 = $user->role_id;
    	$this->user_name            = $user->user_name;
    	$this->status               = $user->status;
    	$this->action               = $action;
    	$this->edit                 = 2;
    }

    public function handleReset($action)
    {
        $this->name                 = '';
        $this->user                 = '';
        $this->email                = '';
        $this->search               = '';
        $this->role                 = '';
        $this->user_name            = '';
        $this->password             = '';
        $this->edit                 = 1;
        $this->action               = $action;
        $this->selected_id          = null;
        $this->newPassword          = null;
        $this->repeatNewPassword    = null;
        $this->role                 = 'Elegir';
        $this->status               = 'ACTIVE';
    }

    public function updatingSearch(){
        $this->gotoPage(1);
    }
}
