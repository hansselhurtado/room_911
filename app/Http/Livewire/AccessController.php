<?php

namespace App\Http\Livewire;

use App\Imports\EmployeeImport;
use App\Models\AccessAction;
use App\Models\Department;
use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Livewire\Component;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;
use Livewire\WithFileUploads;
use PDF ;

class AccessController extends Component
{
    use WithPagination, WithFileUploads;

    protected $paginationTheme = 'bootstrap';//be able to use pagination in laravel 8


    public $selected_id, $search, $action = 1, $count = 1, $edit = 1, $url;
    public $employee, $first_name, $middle_name, $last_name, $document_number, $department = 'Elegir', $status = 'ACTIVE';
    public $departments, $accessEmployee, $csvFile;
    public $dateFromFilter, $dateToFilter, $departmentFilter = 'Elegir', $dateFromFilterShow, $dateToFilterShow;
    public $pagination = 10;

    public function mount(){
        $this->departments  = Department::active()->orderBy('name')->get();
        $this->url          = Route::current()->getName();
    }

    public function render()
    {
        try {
            $employees = Employee::handleAll();

            return view('livewire.employees.component',[
                'info'  => $this->handleFilter($employees)->orderBy('id','asc')->paginate($this->pagination),
            ]);

        } catch (\Exception $e) {
           return $e;
        }
    }

    public function handleAction($action)
    {
        $this->handleReset($action);
    }

    public function StoreOrUpdate($action)
    {
        $this->validate([
                'first_name'                => 'bail|required|min:3|max:20|string',
                'middle_name'               => 'bail|nullable|string|min:3|max:50',
                'last_name'                 => 'bail|required|string|min:3|max:50',
                'document_number'           => 'bail|required|numeric|digits_between:7,12|unique:employees,document_number,'.$this->selected_id,
                'department'                => 'bail|required|not_in:Elegir',
            ]
        );

        $find = [
            'id'  => $this->selected_id,
        ];
        $data = [
            'first_name'            => $this->first_name,
            'last_name'             => $this->last_name,
            'middle_name'           => $this->middle_name ? $this->middle_name : null,
            'document_number'       => $this->document_number,
            'department_id'         => $this->department,
        ];

        // dd($data);
        $employee = Employee::updateOrCreate($find, $data);


        if ($this->selected_id) {
            $updateOrCreate = 'update';
        }else{
            $updateOrCreate = 'created';
        }
        $this->handleReset($action);
        $this->emit('modalsClosed');//emit to js closed modals
        $this->emit('msgok','Employee '.$employee->first_name.', to '.$updateOrCreate);

    }

    // listen for events and execute requested action
    protected $listeners = ['handleState','deleteEmployee'];

    public function handleState(Employee $employe, $status, $action ){
        $employe->status = $status;
        $employe->save();
        $this->handleReset($action);
    }

    public function deleteEmployee(Employee $employe, $action ){
        $employe->delete();
        $this->handleReset($action);
    }

    public function accessEmployee($action){
        if ($this->document_number) {
            $employee = Employee::documenNumber($this->document_number)->first();
            if ($employee) {
                if ($employee->status == 'ACTIVE') {
                    $data = [
                        'employee_id'   => $employee->id,
                        'success'       => 'YES',
                    ];
                    $this->emit('msgok','Access Successful');
                }else{
                    $data = [
                        'employee_id'   => $employee->id,
                    ];
                    $this->emit('msg-error','Employee are DISABLED');
                }

            }else{
                $data = [
                    'document_number'   => $this->document_number,
                ];
                $this->emit('msg-error','Employee does not exist');
            }
            AccessAction::create($data);
            $this->emit('modalsClosed');//emit to js closed modals
            $this->handleReset($action);
        }else{
            $this->emit('msg-error','document number required');
        }

    }

    public function edit(Employee $user, $action)
    {
    	$this->employee             = $user;
    	$this->selected_id          = $user->id;
    	$this->first_name           = $user->first_name;
    	$this->last_name            = $user->last_name;
    	$this->middle_name          = $user->middle_name;
    	$this->document_number      = $user->document_number;
    	$this->department           = $user->department_id;
        $this->accessEmployee       = $user->accessActions;
    	$this->action               = $action;
    	$this->edit                 = 2;
    }

    public function handleReset($action)
    {
        $this->first_name           = '';
        $this->last_name            = '';
        $this->middle_name          = '';
        $this->search               = '';
        $this->document_number      = '';
        $this->department           = '';
        $this->dateFromFilter       = '';
        $this->dateToFilter         = '';
        $this->dateFromFilterShow   = '';
        $this->dateToFilterShow     = '';
        $this->accessEmployee       = '';
        $this->csvFile              = '';
        $this->count                = 1;
        $this->edit                 = 1;
        $this->action               = $action;
        $this->selected_id          = null;
        $this->department           = 'Elegir';
        $this->departmentFilter     = 'Elegir';
        $this->status               = 'ACTIVO';
    }

    public function updatingSearch(){
        $this->gotoPage(1);
    }

    public function handleAccessDate($action){
        $access = AccessAction::getAccesEmployye($this->selected_id);

        if ($this->dateFromFilterShow && $this->dateToFilterShow ) {
            $access->dateAccess($this->dateFromFilterShow, $this->dateToFilterShow);
        }else{
            if ($this->dateFromFilterShow) {
                $access->uniqueDateAccess($this->dateFromFilterShow);
            }
            if ($this->dateToFilterShow) {
                $access->uniqueDateAccess($this->dateToFilterShow);
            }
        }
        $this->accessEmployee   = $access->get();
        $this->action           = $action;
    }

    public function handlePDF(){
        $access = AccessAction::all();
        $date   = Carbon::now()->format('y-m-d - h:i:s');
        $pdf    = PDF::loadView('historyPDF',compact('access', 'date'));
        return $pdf->stream("access-history-{$date}.pdf");
    }

    public function createEmployeeCSV($action){
        if ($this->csvFile) {
            if ($this->csvFile->extension() == 'xlsx' || $this->csvFile->extension() == 'csv') {
                (new EmployeeImport)->import($this->csvFile);
                $this->emit('msgok','Successfull File csv');
                $this->emit('modalsClosed');//emit to js closed modals
                $this->handleReset($action);
            }else{
                $this->emit('msg-error','Unsupported file');
            }
        }else{
            $this->emit('msg-error','File csv not exist');
        }
    }

    public function handleFilter($employee){
        if ($this->search) {
            $employee->search($this->search);
        }
        if ($this->departmentFilter != 'Elegir') {
            $employee->department($this->departmentFilter);
        }

        if ($this->dateFromFilter && $this->dateToFilter ) {
            $employee->dateAccess($this->dateFromFilter, $this->dateToFilter);
        }else{
            if ($this->dateFromFilter) {
                $employee->uniqueDateAccess($this->dateFromFilter);
            }
            if ($this->dateToFilter) {
                $employee->uniqueDateAccess($this->dateToFilter);
            }
        }

        return $employee;
    }
}
