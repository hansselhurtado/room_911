<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function username()
    {
        return 'user_name';
    }

    public function login(Request $request){
        if(Auth::attempt(['user_name' => $request->user_name, 'password' => $request->password])){
            if(Auth::user()->status == "DISABLED"){
                Auth::logout();
                return redirect()->route('login')->with('message','Your user account is inactive');
            }else{
                $user               = User::find(Auth::user()->id);
                $user->last_access  = Carbon::now()->format('y-m-d h:i:s A');
                $user->save();
                return redirect()->route('home');
            }
        }
        return redirect()->route('login')->with('message', 'Invalid credentials');
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
