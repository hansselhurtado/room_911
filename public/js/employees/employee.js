
const handleState = (id, status, action) => {
    if (status == 'ACTIVE') {
        status = 'DISABLED'
    }else{
        status = 'ACTIVE'
    }

    Swal.fire({
        title: '¿Change Status?',
        text: "¡I can change it every time!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, ¿'+`${status}`+'?'
        }).then((result) => {
        if (result.isConfirmed) {
            window.livewire.emit('handleState', id, status, action)
            Swal.fire(
            'Updated!',
            'been successfully updated',
            'success'
            )
        }
    })
}

const deleteEmployee = (id, action) => {

    Swal.fire({
        title: '¿Delete employee?',
        text: "¡you can't change this action!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete'
        }).then((result) => {
        if (result.isConfirmed) {
            window.livewire.emit('deleteEmployee', id, action)
            Swal.fire(
            'Delete!',
            'been successfully delete',
            'success'
            )
        }
    })
}
