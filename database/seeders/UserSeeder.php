<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id'                => 1,
            'name'              => 'Administrador',
            'email'             => 'admin@email.com',
            'role_id'           =>  Role::all()->first()['id'],
            'user_name'         => 'Admin',
            'password'          => Hash::make('Admin@Room_911'),
        ]);
    }
}
